package protoexample

//import scala.concurrent._
import scala.actors._

object misc {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(130); 
  println("Welcome to the Scala worksheet");$skip(54); 
  
  //val webUrl = Config.webApiUrl
  val `then` = 1;System.out.println("""then  : Int = """ + $show(then ));$skip(34); 
  
  
  var x: Option[Int] = None;System.out.println("""x  : Option[Int] = """ + $show(x ));$skip(17); 
  
  x = Some(1);$skip(72); 
  
  //var s: String= null
    
  //Option(s)
  
  val y = x map (_ +1);System.out.println("""y  : Option[Int] = """ + $show(y ));$skip(29); val res$0 = 
  
  for( y <- x)  yield y+1;System.out.println("""res0: Option[Int] = """ + $show(res$0));$skip(71); 
  
  val fx: Future[Int] = Futures.future[Int]({ Thread.sleep(30); 1});System.out.println("""fx  : scala.actors.Future[Int] = """ + $show(fx ));$skip(53); 
                 
  val fy = for( i <- fx) yield i+1;System.out.println("""fy  : Responder[Int] = """ + $show(fy ));$skip(10); val res$1 = 
  
  fx();System.out.println("""res1: Int = """ + $show(res$1));$skip(44); 
  
  val e: Either[Int,String] = Right("q");System.out.println("""e  : Either[Int,String] = """ + $show(e ));$skip(39); 
  val e1: Either[Int,String] = Left(0);System.out.println("""e1  : Either[Int,String] = """ + $show(e1 ));$skip(61); val res$2 = 
  
  for(x <- e.right;
      x1 <- e1.right) yield x+x1+"22";System.out.println("""res2: Either[Int,java.lang.String] = """ + $show(res$2));$skip(59); val res$3 = 
  
  e.right.flatMap(x => e1.right.map(x1 => x+x1+"22"  ));System.out.println("""res3: Either[Int,java.lang.String] = """ + $show(res$3))}
  
  
}