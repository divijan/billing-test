package boot

import java.io.File
import scala.io.Source
import java.io.FileNotFoundException

abstract class Config {
	def webApiUrl : String;
}

class FileConfig extends Config
{
	lazy val webApiUrl : String = fileConfig.getValue("WEBAPIURL")
	
	lazy val fileConfig = new Configuration("/etc/gradsoft/billing-test.param");
}

class Configuration(var FilePath : String)
{
  val Params  = loadParamsFromFile()
  
  def loadParamsFromFile(): scala.collection.mutable.HashMap[String, String] =
  { 
    var outMap = new scala.collection.mutable.HashMap[String, String];    
    
    try  {        
    	val source  = Source.fromFile(FilePath);  	    
        val lines = source.getLines.toArray;        
        
       for (line <- lines) 
       {         
         val valuePair = line.split("=");         
         outMap +=  (valuePair(0) -> valuePair(1));
       }
       
       source.close;
    }
    catch {
      case ex: FileNotFoundException =>
        {
          Console.println("Missing configuration file!")
        }
    }
     outMap
  }
  
  def getValue(key: String) = 
  {
    Params.getOrElse(key, "no_param")
  }
}

class DelegateConfig(var delegate : Config) extends Config
{
   def webApiUrl : String = delegate.webApiUrl;
}

object Config extends DelegateConfig(new FileConfig())
{
  
}