package ua.gradsoft.billing.protocols.radius;


case class VsaIndex(val attribute: Int,
                    val vendor: Int)
                      extends Ordered[VsaIndex]
{
  def compare(that:VsaIndex) =
  {
    val cmp1 = attribute - that.attribute;
    if (cmp1 != 0) cmp1 else vendor - that.vendor;
  }
}

