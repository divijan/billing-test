package ua.gradsoft.billing.protocols.radius;

object VsaHelper
{

   def etherToString(ether: Long): String =
   {
     var v = ether;
     val v8 = (v & 0xff).toInt;
     v >>= 8;
     val v7 = (v & 0xff).toInt;
     v >>= 8;
     val v6 = (v & 0xff).toInt;
     v >>= 8;
     val v5 = (v & 0xff).toInt;
     v >>= 8;
     val v4 = (v & 0xff).toInt;
     v >>= 8;
     val v3 = (v & 0xff).toInt;
     v >>= 8;
     val v2 = (v & 0xff).toInt;
     v >>= 8;
     val v1 = (v & 0xff).toInt;
     val sb=new StringBuilder();
     if (v1!=0 || v2!=0) {
        // usually ether is 6 bytes, so
        //  this is 0-s and must not be writted.
        sb.append(hex((v1>>4)&0x0F))
          .append(hex(v1&0x0F))
          .append(':')
          .append(hex((v2>>4)&0x0F))
          .append(hex(v2&0x0F))
          .append(':')
     }
     sb.append(hex((v3>>4)&0x0F))
       .append(hex(v3&0x0F))
       .append(':')
       .append(hex((v4>>4)&0x0F))
       .append(hex(v4&0x0F))
       .append(':')
       .append(hex((v5>>4)&0x0F))
       .append(hex(v5&0x0F))
       .append(':')
       .append(hex((v6>>4)&0x0F))
       .append(hex(v6&0x0F))
       .append(':')
       .append(hex((v7>>4)&0x0F))
       .append(hex(v7&0x0F))
       .append(':')
       .append(hex((v8>>4)&0x0F))
       .append(hex(v8&0x0F));
      sb.toString;
   }

   def ipToString(ip: Long): String =
   {
     var v = ip;
     val v4 = v & 0xFF;
     v >>= 8;
     val v3 = v & 0xFF;
     v >>= 8;
     val v2 = v & 0xFF;
     v >>= 8;
     val v1 = v & 0xFF;
     val sb = new StringBuilder();
     sb.append(v1.toString)
       .append('.')
       .append(v2.toString)
       .append('.')
       .append(v3.toString)
       .append('.')
       .append(v4.toString)
       .toString;
   }
   
   def normalizeIp(ip:Int):Long =
   {
     var v = ip;
     val v4 = v & 0xFF; v>>=8; 
     val v3 = v & 0xFF; v>>=8; 
     val v2 = v & 0xFF; v>>=8; 
     val v1 = v & 0xFF; v>>=8; 
     (((((v1.toLong << 8)+v2)<<8)+v3)<<8)+v4;
   }
   
   def hex:Array[Char] = Array('0','1','2','3','4','5','6','7','8','9',
                               'a','b','c','d','e','f'); 

   /**
    * try to extract ip from string.
    * 
    **/
   def string2ip(s: String): Either[String,Long] =
   {
     val parts = s.split('.');
     if (parts.length!=4) {
        return Left("must be 4 dots in ip(%s) ".format(s));
     }
     try {
       val v0 = Integer.parseInt(parts(0)).toLong;
       val v1 = Integer.parseInt(parts(1));
       val v2 = Integer.parseInt(parts(2));
       val v3 = Integer.parseInt(parts(3));
       return Right((((((v0<<8)+v1)<<8)+v2)<<8)+v3);
     } catch {
       case ex: NumberFormatException => Left("Invalid ip:"+s);
     }
   }

   def string2ether(s: String): Either[String, Long] =
   {
     val parts = s.split(':');
     if (parts.length != 6)
     {
       return Left("must be 6 dots in ether(%s)".format(s));
     }
     else
     {
       try
       {
         val v0 = Integer.parseInt(parts(0), 16);
         val v1 = Integer.parseInt(parts(1), 16);
         val v2 = Integer.parseInt(parts(2), 16);
         val v3 = Integer.parseInt(parts(3), 16);
         val v4 = Integer.parseInt(parts(4), 16);
         val v5 = Integer.parseInt(parts(5), 16);
         return Right(((((((((v0<<8)+v1)<<8)+v2)<<8)+v3)<<8)+v4<<8)+v5);
       } catch
       {
         case ex: NumberFormatException => Left("Invalid ether:" + s); 
       }
       
     }
   }
   
   def octetsToString(buf: com.google.protobuf.ByteString, maxLen: Int): String =
   {
     octetsToString(buf.toByteArray, maxLen);
   }

   def octetsToString(arr: Array[Byte], maxLen: Int): String =
   {
     val sb = new StringBuilder();
     for(b <- arr) {
        val i:Int = if (b < 0) 256+b else b ;
        sb.append(hex((i>>4)&0x0F));
        sb.append(hex(i&0x0F));
     }
     val l = sb.toString
     val retval = if (l.length > maxLen) {
                      l.substring(0,maxLen);
                  } else {
                      l
                  }
     retval;
   }

}
