package ua.gradsoft.billing.protocols.radius;

object RadiusConstants
{

    val PW_USER_NAME = 1;
    lazy val VSA_USER_NAME = VsaIndex(PW_USER_NAME,0);

    val PW_NAS_PORT = 5;
    val PW_SERVCE_TYPE = 6;
    val PW_FRAMED_IP_ADDRESS = 8;

    val DHCP = 54;
    val DHCP_MESSAGE_TYPE = VsaIndex(53, DHCP);
    val DHCP_MESSAGE_TYPE_DISCOVER = 1.toByte;
    val DHCP_MESSAGE_TYPE_OFFER = 2.toByte;
    val DHCP_MESSAGE_TYPE_REQUEST = 3.toByte;
    val DHCP_MESSAGE_TYPE_DECLINE = 4.toByte;
    val DHCP_MESSAGE_TYPE_ACK = 5.toByte;
    val DHCP_MESSAGE_TYPE_NAK = 6.toByte;
    val DHCP_MESSAGE_TYPE_RELEASE = 7.toByte;
    val DHCP_MESSAGE_TYPE_INFORM = 8.toByte;
    val DHCP_MESSAGE_TYPE_FORCE_RENEW = 9.toByte;

    val DHCP_REQUESTED_IP_ADDRESS = VsaIndex(50, DHCP);

    val DHCP_TRANSACTION_ID = VsaIndex(260,DHCP);
    val DHCP_IP_ADDRESS_LEASE_TIME = VsaIndex(51, DHCP);
    val DHCP_RELAY_CIRCUIT_ID = VsaIndex(256+82,DHCP);
    val DHCP_CLIENT_IP_ADDRESS = VsaIndex(263,DHCP);
    val DHCP_YOU_IP_ADDRESS = VsaIndex(264,DHCP);
    val DHCP_CLIENT_HARDWARE_ADDRESS = VsaIndex(267,DHCP);
    val DHCP_SUBNET_MASK = VsaIndex(1,DHCP);

}
