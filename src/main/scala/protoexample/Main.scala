package protoexample

import org.freeradius._
import org.freeradius.Vsa._
import ua.gradsoft.billing.protocols.radius.RadiusConstants._
import ua.gradsoft.billing.protocols.radius._
import scala.xml._
import dispatch._
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent._
import scala.concurrent.duration._
import boot._

sealed trait DHCPClientState
case object INIT extends DHCPClientState
case object SELECTING extends DHCPClientState
case object REQUESTING extends DHCPClientState
case object INIT_REBOOT extends DHCPClientState
case object REBOOTING extends DHCPClientState
case object BOUND extends DHCPClientState
case object RENEWING extends DHCPClientState
case object REBINDING extends DHCPClientState

class DHCPClient (var state: DHCPClientState = INIT) {
  def discover() = {
    val bytesToSend = RequestData(protocolVersion = 2, //factor out
               state = ProcessingState.POSTAUTH, //probably factor out
               vps = Vector(
                   ValuePair(
                       attribute=DHCP_MESSAGE_TYPE.attribute,
                       vendor=Some(DHCP),
                       byteValue=Some(DHCP_MESSAGE_TYPE_DISCOVER)
                   )
               )
    ).toByteArray
    //RequestData.parseFrom(//input stream)
    val f = Http( url(Config.webApiUrl).PUT.setBody(bytesToSend) OK as.String)
   
    Await.result(f map { s => System.out.println("received: " + s ) }, 10.second );
  }
    
}

object Main
{

  def main(args: Array[String]):Unit =
  {
    Console.println("Hello!")
    val user1 = new DHCPClient()
    user1.discover() 
  }
 
}
